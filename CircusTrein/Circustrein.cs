﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CircusTrein.Classes;
using CircusTrein.Enums;

namespace CircusTrein
{
    [ExcludeFromCodeCoverage]
    public partial class Circustrein : Form
    {
        
        public Circustrein()
        {
            InitializeComponent();

            var Animals = new List<Animal>();
            
            Animals.Add(new Animal(AnimalType.Carnivore, AnimalScale.Big));
            Animals.Add(new Animal(AnimalType.Carnivore, AnimalScale.Big));
            Animals.Add(new Animal(AnimalType.Carnivore, AnimalScale.Medium));
            Animals.Add(new Animal(AnimalType.Carnivore, AnimalScale.Medium));
            Animals.Add(new Animal(AnimalType.Carnivore, AnimalScale.Medium));
            Animals.Add(new Animal(AnimalType.Carnivore, AnimalScale.Medium));
            Animals.Add(new Animal(AnimalType.Carnivore, AnimalScale.Small));
            Animals.Add(new Animal(AnimalType.Carnivore, AnimalScale.Small));
            Animals.Add(new Animal(AnimalType.Carnivore, AnimalScale.Small));
            Animals.Add(new Animal(AnimalType.Carnivore, AnimalScale.Small));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Big));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Big));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Big));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Big));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Big));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Medium));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Medium));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Medium));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Medium));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Medium));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Small));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Small));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Small));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Small));
            Animals.Add(new Animal(AnimalType.Herbivore, AnimalScale.Small));

            var SortedAnimals = Animals.OrderByDescending(animal => animal.AnimalType).ToList();
            
            
            var trainManager = new TrainManager();
            var wagons = trainManager.Calculate(SortedAnimals);
        }
    }
}
