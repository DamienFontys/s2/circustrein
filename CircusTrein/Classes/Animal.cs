﻿using CircusTrein.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircusTrein.Classes
{
    public class Animal
    {
        public AnimalType AnimalType { get; private set; }
        public AnimalScale AnimalScale { get; private set; }

        public Animal(AnimalType type, AnimalScale scale)
        {
            AnimalType = type;
            AnimalScale = scale;
        }
        
    }
}
