﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CircusTrein.Enums;

namespace CircusTrein.Classes
{
    public class Wagon
    {
        public int MaxWeight { get; private set; }
        public int CurrentWeight { get; private set; }

        public List<Animal> Animals { get; private set; }

        public Wagon()
        {
            Animals = new List<Animal>();
            MaxWeight = 10;
        }

        public Wagon(Animal animal)
        {
            Animals = new List<Animal>();
            MaxWeight = 10;
            Animals.Add(animal);
            CurrentWeight += (int)animal.AnimalScale;
        }

        public bool AddAnimal(Animal animal)
        {
            if ((int) animal.AnimalScale > (MaxWeight - CurrentWeight))
            {
                return false;
            }
            
            //check if carnivore is being added and already has carnivore
            if(animal.AnimalType == Enums.AnimalType.Carnivore && HasCarnivore())
            {
                return false;
            }
            
            if(animal.AnimalType == Enums.AnimalType.Carnivore && HasSmallerHerbivore(animal))
            {
                return false;
            }
            
            if(animal.AnimalType == Enums.AnimalType.Herbivore && HasBiggerCarnivore(animal))
            {
                return false;
            }

            //add animal
            Animals.Add(animal);
            CurrentWeight += (int) animal.AnimalScale;
            return true;
        }

        private bool HasSmallerHerbivore(Animal animal)
        {
            var herbivores = Animals.FindAll(anim => anim.AnimalType == Enums.AnimalType.Herbivore);
            if(herbivores.Count > 0)
            {
                foreach (var herbivore in herbivores) {
                    if((int)herbivore.AnimalScale <= (int)animal.AnimalScale)
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }

        private bool HasBiggerCarnivore(Animal animal)
        {
            var carnivores = Animals.FindAll(anim => anim.AnimalType == Enums.AnimalType.Carnivore);
            if(carnivores.Count > 0)
            {
                foreach(var carnivore in carnivores)
                {
                    if((int)carnivore.AnimalScale >= (int)animal.AnimalScale)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        
        public bool HasCarnivore()
        {
            return Animals.Any(animal => animal.AnimalType == Enums.AnimalType.Carnivore);
        }

    }
}
