﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircusTrein.Enums
{
    public enum AnimalScale
    {
        Small=1,
        Medium=3,
        Big=5
    }
}
