using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using CircusTrein.Classes;

namespace CircusTrein
{
    public class TrainManager
    {
        public List<Wagon> Calculate(List<Animal> animals)
        {
            var wagons = new List<Wagon> { new Wagon() };

            foreach(var animal in animals)
            {   
                //Check if there are wagons with space
                var foundwagons = WagonsWithSpace(wagons, animal);
                if(foundwagons.Count > 0)
                {
                    //Loop through the wagons and try to add the animal
                    if (!TryAddAnimal(foundwagons, animal))
                    {
                        wagons.Add(new Wagon(animal));   
                    }
                }
                //if no wagons with space are found -> create a new wagon with the animal
                else
                {
                    wagons.Add(new Wagon(animal));
                }
            }

            return wagons;
        }

        public List<Wagon> WagonsWithSpace(List<Wagon> wagons, Animal animal)
        {
            return wagons.FindAll(wagon => (wagon.MaxWeight - wagon.CurrentWeight) >= (int)animal.AnimalScale);
        }

        public bool TryAddAnimal(List<Wagon> wagonsWithSpace, Animal animal)
        {
            foreach(var wagon in wagonsWithSpace)
            {
                if (wagon.AddAnimal(animal))
                {
                    return true;
                }
            }

            return false;
        }
        
    }
}