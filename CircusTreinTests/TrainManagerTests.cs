using System.Collections.Generic;
using CircusTrein;
using CircusTrein.Classes;
using CircusTrein.Enums;
using Xunit;

namespace CircusTreinTests
{
    public class TrainManagerTests
    {
        [Fact]
        public void GettingWagonsWithSpace_WithNoWagonsWithSpace_ReturnsAnEmptyList()
        {
            //Arrange
            var trainManager = new TrainManager();
            var animal1 = new Animal(AnimalType.Herbivore, AnimalScale.Big);
            var animal2 = new Animal(AnimalType.Herbivore, AnimalScale.Big);
            var animal3 = new Animal(AnimalType.Herbivore, AnimalScale.Big);
            var wagon = new Wagon();
            var wagons = new List<Wagon> {wagon};

            //Act
            wagon.AddAnimal(animal1);
            wagon.AddAnimal(animal2);
            var wagonsWithSpace = trainManager.WagonsWithSpace(wagons, animal3);
                
            //Assert
            Assert.Empty(wagonsWithSpace);
        }

        [Fact]
        public void GettingWagonsWithSpace_WithAtleastOneWagonWithSpace_Returns_A_List_With_Wagons()
        {
            //Arrange
            var trainManager = new TrainManager();
            var animal1 = new Animal(AnimalType.Herbivore, AnimalScale.Medium);
            var animal2 = new Animal(AnimalType.Herbivore, AnimalScale.Medium);
            var wagon = new Wagon();
            var wagons = new List<Wagon> {wagon};
            
            //Act
            wagon.AddAnimal(animal1);
            var wagonsWithSpace = trainManager.WagonsWithSpace(wagons, animal2);

            //Assert
            Assert.NotEmpty(wagonsWithSpace);
            Assert.True(wagonsWithSpace.Contains(wagon));
        }

        [Fact]
        public void TryAddingAnimals_WhenAnAnimalCantBeAdded_ReturnsFalse()
        {
            //Arrange
            var trainManager = new TrainManager();
            var animal1 = new Animal(AnimalType.Herbivore, AnimalScale.Medium);
            var animal2 = new Animal(AnimalType.Carnivore, AnimalScale.Big);
            var wagon = new Wagon();
            var wagons = new List<Wagon> {wagon};

            //Act
            wagon.AddAnimal(animal1);
            var success = trainManager.TryAddAnimal(wagons, animal2);

            //Assert
            Assert.False(success);
        }

        [Fact]
        public void TryAddingAnimals_WhenAnAnimalCanBeAdded_ReturnsTrue()
        {
            //Arrange
            var trainManager = new TrainManager();
            var animal1 = new Animal(AnimalType.Herbivore, AnimalScale.Medium);
            var animal2 = new Animal(AnimalType.Carnivore, AnimalScale.Small);
            var wagon = new Wagon();
            var wagons = new List<Wagon> {wagon};

            //Act
            wagon.AddAnimal(animal1);
            var success = trainManager.TryAddAnimal(wagons, animal2);

            //Assert
            Assert.True(success);
        }

        [Fact]
        public void CalculatingTheWagons_WithAKnownSetup_ReturnsTheRightAmountOfWagons()
        {
            //Arrange
            var trainManager = new TrainManager();
            var animal1 = new Animal(AnimalType.Carnivore, AnimalScale.Medium);
            var animal2 = new Animal(AnimalType.Carnivore, AnimalScale.Medium);
            var animals = new List<Animal>();
            animals.Add(animal1);
            animals.Add(animal2);
            
            //Act
            var result = trainManager.Calculate(animals);
            
            //Assert
            Assert.True(result.Count == 2);
        }

        [Fact]
        public void CalculatingTheWagons_WithAKnowSetupWithNoFoundWagons_ReturnsTheRightAmountOfwagons()
        {
            //Arrange
            var trainManager = new TrainManager();
            var animal1 = new Animal(AnimalType.Herbivore, AnimalScale.Big);
            var animal2 = new Animal(AnimalType.Herbivore, AnimalScale.Medium);
            var animal3 = new Animal(AnimalType.Herbivore, AnimalScale.Big);
            var animals = new List<Animal>();
            animals.Add(animal1);
            animals.Add(animal2);
            animals.Add(animal3);
            
            //Act
            var result = trainManager.Calculate(animals);
            
            //Assert
            Assert.True(result.Count == 2);
        }
    }
}