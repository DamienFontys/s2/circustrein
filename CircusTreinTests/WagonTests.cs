using System;
using System.Collections.Generic;
using CircusTrein;
using CircusTrein.Classes;
using CircusTrein.Enums;
using Xunit;

namespace CircusTreinTests
{
    public class WagonTests
    {
        [Fact]
        public void AddingAnAnimal_ToAFullWagon_ReturnsFalse()
        {
            //Arrange
            var wagon = new Wagon();
            
            //Act
            wagon.AddAnimal(new Animal(AnimalType.Herbivore, AnimalScale.Big));
            wagon.AddAnimal(new Animal(AnimalType.Herbivore, AnimalScale.Big));
            
            //Assert
            Assert.False(wagon.AddAnimal(new Animal(AnimalType.Herbivore, AnimalScale.Medium)));
        }

        [Fact]
        public void AddingACarnivore_ToAWagonThatHasACarnivore_ReturnsFalse()
        {
            //Arrange
            var wagon = new Wagon();
            
            //Act
            wagon.AddAnimal(new Animal(AnimalType.Carnivore, AnimalScale.Big));
            
            //Assert
            Assert.False(wagon.AddAnimal(new Animal(AnimalType.Carnivore, AnimalScale.Medium)));
        }

        [Fact]
        public void AddingACarnivore_ToAWagonThatHasABiggerHerbivore_ReturnsTrue()
        {
            //Arrange
            var wagon = new Wagon();
            
            //Act
            wagon.AddAnimal(new Animal(AnimalType.Herbivore, AnimalScale.Big));
            
            //Assert
            Assert.True(wagon.AddAnimal(new Animal(AnimalType.Carnivore, AnimalScale.Medium)));
        }

        [Fact]
        public void AddingACarnivore_ToAWagonThatHasASmallerHerbivore_ReturnsFalse()
        {
            //Arrange
            var wagon = new Wagon();
            
            //Act
            wagon.AddAnimal(new Animal(AnimalType.Herbivore, AnimalScale.Small));
            
            //Assert
            Assert.False(wagon.AddAnimal(new Animal(AnimalType.Carnivore, AnimalScale.Medium)));
        }
        
        [Fact]
        public void AddingAHerbivore_ToAWagonThatHasASmallerCarnivore_ReturnsTrue()
        {
            //Arrange
            var wagon = new Wagon();
            
            //Act
            wagon.AddAnimal(new Animal(AnimalType.Carnivore, AnimalScale.Small));
            
            //Assert
            Assert.True(wagon.AddAnimal(new Animal(AnimalType.Herbivore, AnimalScale.Medium)));
        }
        
        [Fact]
        public void AddingAHerbivore_ToAWagonThatHasABiggerCarnivore_ReturnsFalse()
        {
            //Arrange
            var wagon = new Wagon();
            
            //Act
            wagon.AddAnimal(new Animal(AnimalType.Carnivore, AnimalScale.Big));
            
            //Assert
            Assert.False(wagon.AddAnimal(new Animal(AnimalType.Herbivore, AnimalScale.Medium)));
        }

        [Fact]
        public void InstantiatingAWagon_WithAnAnimal_ReturnsAWagonWithAPrefilledList()
        {
            //Arrange
            var animal = new Animal(AnimalType.Carnivore, AnimalScale.Medium);
            
            //Act
            var wagon = new Wagon(animal);
            
            //Assert
            Assert.NotEmpty(wagon.Animals);
        }

        [Fact]
        public void InstantiatingAWagon_WithoutAnAnimal_ReturnsAWagonWithAnEmptyAnimalsList()
        {
            //Arrange
            
            //Act
            var wagon = new Wagon();
            
            //Assert
            Assert.Empty(wagon.Animals);
        }
    }
}